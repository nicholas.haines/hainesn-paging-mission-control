import { findAlerts, isWithinInterval, outOfRangeCountThreshold } from '@/utils/parseInput';

describe('parseInput.js', () => {
  describe('outOfRangeCountThreshold', () => {
    it('BATT count', () => {
      const count = outOfRangeCountThreshold('BATT');
      expect(count).toBe(3);
    });
    it('TSTAT count', () => {
      const count = outOfRangeCountThreshold('TSTAT');
      expect(count).toBe(3);
    });
    it('UNKNOWN count', () => {
      const count = outOfRangeCountThreshold('UNKNOWN');
      expect(count).toBe(-1);
    });
  });
  describe('isWithinInterval', () => {
    it('BATT 5 minutes', () => {
      const start = new Date('2018-01-01T23:00:00.000Z');
      const end = new Date('2018-01-01T23:05:00.000Z');
      const withinInterval = isWithinInterval('BATT', start, end);
      expect(withinInterval).toBe(true);
    });
    it('BATT over 5 minutes', () => {
      const start = new Date('2018-01-01T23:00:00.000Z');
      const end = new Date('2018-01-01T23:05:00.001Z');
      const withinInterval = isWithinInterval('BATT', start, end);
      expect(withinInterval).toBe(false);
    });
    it('TSTAT 5 minutes', () => {
      const start = new Date('2018-01-01T23:00:00.000Z');
      const end = new Date('2018-01-01T23:05:00.000Z');
      const withinInterval = isWithinInterval('TSTAT', start, end);
      expect(withinInterval).toBe(true);
    });
    it('TSTAT over 5 minutes', () => {
      const start = new Date('2018-01-01T23:00:00.000Z');
      const end = new Date('2018-01-01T23:05:00.001Z');
      const withinInterval = isWithinInterval('TSTAT', start, end);
      expect(withinInterval).toBe(false);
    });
    it('UNKNOWN 5 minutes', () => {
      const start = new Date('2018-01-01T23:00:00.000Z');
      const end = new Date('2018-01-01T23:05:00.000Z');
      const withinInterval = isWithinInterval('UNKNOWN', start, end);
      expect(withinInterval).toBe(false);
    });
  });
  describe('findAlerts', () => {
    it('finds a low battery voltage alert - 5 minute interval', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.8,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:02:11.302Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.7,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.9,
          component: 'BATT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([
        {
          satelliteId: 1000,
          severity: 'RED LOW',
          component: 'BATT',
          timestamp: '2018-01-01T23:00:00.000Z',
        },
      ]);
    });
    it('finds a high thermostat alert - 5 minute interval', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:01:49.021Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 87.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:03:03.008Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.7,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 101.2,
          component: 'TSTAT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([
        {
          satelliteId: 1000,
          severity: 'RED HIGH',
          component: 'TSTAT',
          timestamp: '2018-01-01T23:00:00.000Z',
        },
      ]);
    });
    it('does NOT find a low battery voltage alert - more than 5 minutes', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.8,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:02:11.302Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.7,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.001Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.9,
          component: 'BATT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('does NOT find a high thermostat alert - more than 5 minutes', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:03:03.008Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.7,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:06:00.001Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 101.2,
          component: 'TSTAT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('finds a low battery voltage alert - less than 3 values out of range', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.8,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:02:11.302Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 8.7,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.9,
          component: 'BATT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('finds a high thermostat alert - less than 3 values out of range', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:01:49.021Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 87.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:03:03.008Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 100.7,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 101.2,
          component: 'TSTAT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('finds a low battery voltage alert - less than 3 values out of range for the same satellite', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.8,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:02:11.302Z'),
          satelliteId: 1001,
          redLowLimit: 8,
          rawValue: 7.7,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.9,
          component: 'BATT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('finds a high thermostat alert - less than 3 values out of range for the same satellite', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:01:49.021Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 87.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:03:03.008Z'),
          satelliteId: 1001,
          redHighLimit: 101,
          rawValue: 101.7,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 101.2,
          component: 'TSTAT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([]);
    });
    it('finds a low battery voltage alert AND high termostat alert', () => {
      const data = [
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.8,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:02:11.302Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.7,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redLowLimit: 8,
          rawValue: 7.9,
          component: 'BATT',
        },
        {
          timestamp: new Date('2018-01-01T23:00:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:01:49.021Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 87.9,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:03:03.008Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 102.7,
          component: 'TSTAT',
        },
        {
          timestamp: new Date('2018-01-01T23:05:00.000Z'),
          satelliteId: 1000,
          redHighLimit: 101,
          rawValue: 101.2,
          component: 'TSTAT',
        },
      ];
      const alerts = findAlerts(data);
      expect(alerts).toStrictEqual([
        {
          satelliteId: 1000,
          severity: 'RED LOW',
          component: 'BATT',
          timestamp: '2018-01-01T23:00:00.000Z',
        },
        {
          satelliteId: 1000,
          severity: 'RED HIGH',
          component: 'TSTAT',
          timestamp: '2018-01-01T23:00:00.000Z',
        },
      ]);
    });
  });
});
