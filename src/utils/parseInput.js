import { differenceInMinutes, parse } from 'date-fns';
import { zonedTimeToUtc } from 'date-fns-tz';

const COMPONENT = Object.freeze({
  BATT: 'BATT',
  TSTAT: 'TSTAT',
});

const SEVERITY = Object.freeze({
  RED_LOW: 'RED LOW',
  RED_HIGH: 'RED HIGH',
});

/**
 * Returns the severity string to display for a component.
 * @param {string} component - The component
 * @returns The severity string to display for a component.
 */
function calculateSeverity(component) {
  switch (component) {
    case COMPONENT.BATT:
      return SEVERITY.RED_LOW;
    case COMPONENT.TSTAT:
      return SEVERITY.RED_HIGH;
    default:
      return 'UNKNOWN';
  }
}

/**
 * Returns the number of values a component needs to before an alert can be reported.
 * @param {string} component - The component
 * @returns {number} The number of out of range values needed before testing for an alert.
 */
export function outOfRangeCountThreshold(component) {
  switch (component) {
    case COMPONENT.BATT:
      return 3;
    case COMPONENT.TSTAT:
      return 3;
    default:
      return -1;
  }
}

/**
 * Returns if the dates are within the interval for the component.
 * @param {string} component - The component being checked
 * @param {Date} start - The earlier time
 * @param {Date} end - The later time
 * @returns {boolean} True if the dates are within the interval for the provided component.
 */
export function isWithinInterval(component, start, end) {
  // Need to round UP to catch fractional difference
  const difference = differenceInMinutes(end, start, { roundingMethod: 'ceil' });
  switch (component) {
    case COMPONENT.BATT:
      return difference <= 5;
    case COMPONENT.TSTAT:
      return difference <= 5;
    default:
      return false;
  }
}

/**
 * Returns an array of alerts found within the telemetry data.
 * @param {Array} data - The telemetry array data
 * @returns {Array} An array of alerts found.
 */
export function findAlerts(data) {
  const alert = [];
  const tracking = {};
  data.forEach((item) => {
    const {
      satelliteId, component, redHighLimit, redLowLimit, rawValue, timestamp,
    } = item;
    if (!Object.prototype.hasOwnProperty.call(tracking, satelliteId)) {
      tracking[satelliteId] = {};
    }
    const satellite = tracking[satelliteId];
    if (!Object.prototype.hasOwnProperty.call(satellite, component)) {
      satellite[component] = [];
    }
    const current = satellite[component];
    if (component === COMPONENT.BATT && rawValue < redLowLimit) {
      current.push(timestamp);
    } else if (component === COMPONENT.TSTAT && rawValue > redHighLimit) {
      current.push(timestamp);
    }
    if (current.length === outOfRangeCountThreshold(component)) {
      const withinInterval = isWithinInterval(component, current[0], current[2]);
      if (withinInterval) {
        alert.push({
          satelliteId,
          severity: calculateSeverity(component),
          component,
          timestamp: current[0].toISOString(),
        });
        // Alert found
        // Clear the tracked timestamps for this component to find the next alert
        satellite[component] = [];
      } else {
        // Remove first timestap since we are beyond the required interval
        current.shift();
      }
    }
  });
  return alert;
}

/**
 * Creates a UTC Date for the timestamp string.
 * @param {string} timestamp - The timestamp string
 * @returns {Date} A Date object based on the timestamp string in UTC time.
 */
function parseDate(timestamp) {
  const date = parse(timestamp, 'yyyyMMdd HH:mm:ss.SSS', new Date());
  return zonedTimeToUtc(date, 'UTC');
}

/**
 * Parses the telemetry file into an array of Objects.
 * @param {String} text - The telemetry file as a text string
 * @returns {Array} The array of telemetry data as Objects.
 */
export function parseTelemetryText(text) {
  const lines = text.split('\n');
  return lines.map((line) => {
    const [
      timestamp,
      satelliteId,
      redHighLimit,
      yellowHighLimit,
      yellowLowLimit,
      redLowLimit,
      rawValue,
      component,
    ] = line.split('|');
    return {
      timestamp: parseDate(timestamp),
      satelliteId: parseFloat(satelliteId),
      redHighLimit: parseFloat(redHighLimit),
      yellowHighLimit: parseFloat(yellowHighLimit),
      yellowLowLimit: parseFloat(yellowLowLimit),
      redLowLimit: parseFloat(redLowLimit),
      rawValue: parseFloat(rawValue),
      component,
    };
  });
}
